fetch('data.json').then(data => data.json()).then(data => {
    visualize1(data);
    visualize2(data);
    visualize3(data);
    visualize4(data);
})



const visualize1 = (data) => {

    Highcharts.chart('container1', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[0]
        },
        xAxis: {
            categories: Object.keys(data[Object.keys(data)[0]]),
            title: {
                text: 'Year'
            }
        },
        yAxis: {
            min: 40,
            title: {
                text: 'Number of matches played'
            }
        },
        tooltip: {
            valueSuffix: ' matches'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'IPL Chart-1',
            data: Object.values(data[Object.keys(data)[0]]),
            colorByPoint: true
        }]
    });
}

const visualize2 = (data) => {

    Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[2]
        },
        xAxis: {
            categories: Object.keys(data[Object.keys(data)[2]]),
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            min: 80,
            title: {
                text: 'Extra runs'
            }
        },
        tooltip: {
            valueSuffix: ' runs'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'IPL Chart-2',
            data: Object.values(data[Object.keys(data)[2]]),
            "colorByPoint": true
        }]
    });
}

const visualize3 = (data) => {

    Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: Object.keys(data)[3]
        },
        xAxis: {
            categories: Object.keys(data[Object.keys(data)[3]]),
            title: {
                text: 'Bowlers'
            }
        },
        yAxis: {
            min: 3,
            title: {
                text: 'Economy'
            }
        },
        tooltip: {
            valueSuffix: ' Runs per Over'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'IPL Chart-3',
            data: Object.values(data[Object.keys(data)[3]]),
            "colorByPoint": true
        }]
    });
}

const visualize4 = (data) => {

    let arr = Object.values(data[Object.keys(data)[1]]);
    let arrs = [];
    let year = 2008;
    let i = 0;    
    for (let el of arr) {        
        arrs[i] = {};
        arrs[i].name = year.toString();
        year++;       
        arrs[i].data = [];
        for (let e of arr) {
            arrs[i].data.push(Object.values(e)[i]);
        }        
        i++;
    }
    Highcharts.chart('container4', {
        chart: {
            type: 'bar'
        },
        title: {
            text: Object.keys(data)[1]
        },
        xAxis: {
            categories: Object.keys(data[Object.keys(data)[1]]),
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of matches won'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series : arrs
    });
}